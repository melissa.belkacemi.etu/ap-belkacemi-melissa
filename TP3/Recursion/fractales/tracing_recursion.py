#Melissa Belkacemi
#TP3
#31/01/2024

# Tracer une fonction récursive
from ap_decorators import trace
@trace
def somme(a:int,b:int)-> int :
    """Renvoie la somme de 2 entiers naturels

    Précondition : a>=0 et b>=0 
    Exemple(s) :
    $$$ somme(0,4)
    4
    $$$ somme(3,4)
    7

    """
    if a==0:
        return b
    elif a>0:
        return somme(a-1,b+1)

def binomial(n:int,p:int)-> float:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : n>=p>=0
    Exemple(s) :
    $$$ binomial(2,2)
    1
    $$$ binomial(2,0)
    1
    $$$ binomial(3,2)
    3
    
    """
    if p==0 or p==n:
        return 1
    else:
        return binomial(n-1,p-1)+binomial(n-1,p)
    
def is_palindromic(mot:str)->bool:
    """Renvoie True si mot est un palindrome

    Précondition :len(mot)>0 
    Exemple(s) :
    $$$ is_palindromic("radar")
    True
    $$$ is_palindromic("elle")
    True
    $$$ is_palindromic("mot")
    False

    """
    if len(mot)<=1 :
        return True
    
    else:
        return (mot[0]==mot[-1] and is_palindromic(mot[1:-1]))
    