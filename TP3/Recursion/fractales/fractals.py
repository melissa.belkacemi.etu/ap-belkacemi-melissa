#Melissa Belkacemi
#TP3
#31/01/2024

#Dessins de fractales

import turtle
    

def trca_courbe(l:int,n:int):
    """Dessine le flocon de Von Koch d'ordre n

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l/3)
    else:
        trca_courbe(l/3,n-1)
        turtle.left(60)
        trca_courbe(l/3,n-1)
        turtle.right(120)
        trca_courbe(l/3,n-1)
        turtle.left(60)
        trca_courbe(l/3,n-1)
        
#Courbe de Cesaro
     
def trca_courbe_cesaro(l:int,n:int):
    """Dessine la Courbe de Cesaro d'ordre n

    Précondition :/ 
    Exemple(s) :/

    """
    if n==0:
        turtle.forward(l/3)
    else:
        trca_courbe_cesaro(l/3,n-1)
        turtle.left(85)
        trca_courbe_cesaro(l/3,n-1)
        turtle.right(170)
        trca_courbe_cesaro(l/3,n-1)
        turtle.left(85)
        trca_courbe_cesaro(l/3,n-1)
    
        
        
    
    