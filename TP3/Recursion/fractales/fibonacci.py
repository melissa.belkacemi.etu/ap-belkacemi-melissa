#Melissa Belkacemi
#TP3
#31/01/2024

# Nombres de Fibonacci
from ap_decorators import count
@count
def fibo(n:int)->int:
    """Renvoie le nombre de fibonacci 

    Précondition :n>=0 
    Exemple(s) :
    $$$ fibo(0)
    0
    $$$ fibo(1)
    1
    $$$ fibo(10)
    55
    

    """
    if n<=1:
        return n
    else:
        return fibo(n-1)+fibo(n-2)
    
# fibo(40) prend beaucoup de temps
