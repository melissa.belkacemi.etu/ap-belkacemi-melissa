#Belkacemi Melissa
#   31/03/2024

def couleur_moyenne(liste:list[tuple[int,int,int]])->tuple[int,int,int]:
    """déterminer la couleur moyenne d'une liste de couleurs sous la forme (r,g,b)

    Précondition : len(liste)>0 r,v et b <=255
    Exemple(s) :
    $$$ couleur_moyenne([(1,32,4)])
    (1,32,4)
    $$$ couleur_moyenne([(2,32,4),(0,4,6)])
    (1,18,5)
    $$$ couleur_moyenne([(2,32,4),(0,4,6),(8,5,4)])
    (3,13,4)

    """
    rouge=0
    vert=0
    bleu=0
    for elt in liste:
        rouge+=elt[0]
        vert+=elt[1]
        bleu+=elt[2]
    return (rouge//len(liste),vert//len(liste),bleu//len(liste))
    
def coul_sont_proches(coul1:tuple[int,int,int],coul2:tuple[int,int,int])->bool:
    """Prend en paramètres deux couleurs (r1,v1,b1) et (r2,v2,b2) et
    renvoie True si les distances entre r1 et r2, v1 et v2, et b1 et b2
    est inférieure à 10 

    Précondition : les couleurs sont correctes
    Exemple(s) :
    $$$ coul_sont_proches((145,145,130),(140,136,132))
    True
    $$$ coul_sont_proches((45,45,30),(45,36,32))
    True
    $$$ coul_sont_proches((40,45,32),(40,36,32))
    True
    $$$ coul_sont_proches((45,45,30),(40,36,46))
    False
    $$$ coul_sont_proches((10,45,30),(40,36,32))
    False

    """
    r1,v1,b1=coul1
    r2,v2,b2=coul2
    return (abs(r1-r2)<=10 and abs(v1-v2)<=10 and abs(b1-b2)<=10)
