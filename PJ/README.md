Title:Projet Images Récurssives
---
Author: Belkacemi Melissa
---
#journal
06/03/2024: 

- lecture du sujet du projet
- création du document images


13/03/2024:

- création du fichier couleur.py
- création du fichier bloc.py

20/03/2024:

- j'ai essayé d'arranger un problème dans la classe bolc sans succès


27/03/2024:

- j'ai arrangé ma classe bloc 


29/03/2024:


- ajout du fichier decoupe.py


31/03/2024:


- ajout de documentation dans la classe bloc 
- moddification de la classe bloc et des fonctions decoupe et quatre_blocs


01/04/2024:


- ajout des fonctions ouvre_image dessine_blocs et dessine dans le fichier decoupe.py
- ajout de la ligne de commande
- mise à jours du document README


#Documentation 

couleur.py:


- couleur_moyenne:détermine la couleur moyenne d'une liste de couleurs sous la forme (r,g,b)
- coul_sont_proches: Prend en paramètres deux couleurs (r1,v1,b1) et (r2,v2,b2) et renvoie True si les distances entre r1 et r2, v1 et v2, et b1 et b2 est inférieure à 10 


bloc.py:

- sont_proches:Renvoie True si les couleurs des 4 blocs sont proches


decoupe.py:


- decoupe: divise une image en blocs
- quatre_blocs: découpe une image en 4 blocs
- ouvre_image: ouvre et convertit en formay RGB l'image prise en paramètres
- dessine_blocs: dessine le bloc dans l'image
- dessine: associe à un bloc une image bitmap le représentant


#Exemples d'utilisation 

sur le terminal écrire:


- pour afficher une image: decoupe.py chemin vers l'image ordre affiche
- our enregistrer une image: decoupe.py chemin vers l'image ordre enregistre


chemin vers l'image et ordre représentent le chemin vers l'image et l'ordre souhaités
