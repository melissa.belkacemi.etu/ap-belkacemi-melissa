# Belkacemi Melissa
# 01/03/2024
from bloc import *
from couleur import *
from PIL import Image, ImageDraw




def decoupe(image:Image,p_hg,p_bd,n:int)->Bloc:
    """divise une image en blocs

    Précondition :n>=0 
    
    """
     
    if n >0:
        b1,b2,b3,b4=quatre_blocs(image,hg,bd)
        decoupe(image,b1.pixel_hg,b1.pixel_bd,n-1)
        decoupe(image,b2.pixel_hg,b2.pixel_bd,n-1)
        decoupe(image,b3.pixel_hg,b3.pixel_bd,n-1)
        decoupe(image,b4.pixel_hg,b4.pixel_bd,n-1)
        if b1.sont_proches(b2,b3,b4):
            return(Bloc(couleur_moyenne([b1.couleur,b2.couleur,b3.couleur,b4.couleur]),b1.pixel_hg,b4.pixel_bd))
        else:
            return(Bloc((0,0,0),b1.pixel_hg,b4.pixel_bd,b1,b2,b3,b4))
    else:
        couleurs=[image.getpixel((i,j)) for i in range(p_hg[0],1+p_bd[0]) for j in range(p_hg[1],1+p_bd[1])]
        coul_moyenne=couleur_moyenne(couleurs)
        return Bloc(coul_moyenne,p_hg,p_bd)
        
 
        

def quatre_blocs(im:Image,hg:tuple[int,int],bd:tuple[int,int])->tuple[Bloc,Bloc,Bloc,Bloc]:
    """découpe une image en 4 blocs

    """
    x1,y1=hg
    x2,y2=bd
    pixel_hg1=hg
    pixel_bd1=((x1+x2)//2,(y1+y2)//2)
    pixel_hg2=(1+(x1+x2)//2,y1)
    pixel_bd2=(x2,(y1+y2)//2)
    pixel_hg3=(x1,1+(y1+y2)//2)
    pixel_bd3=((x1+x2)//2,y2)
    pixel_hg4=(1+(x1+x2)//2,1+(y1+y2)//2)
    pixel_bd4=bd
    couleurs1=[im.getpixel((i,j)) for i in range(pixel_hg1[0],1+pixel_bd1[0]) for j in range(pixel_hg1[1],1+pixel_bd1[1])]
    couleurs2=[im.getpixel((i,j)) for i in range(pixel_hg2[0],1+pixel_bd2[0]) for j in range(pixel_hg2[1],1+pixel_bd2[1])]
    couleurs3=[im.getpixel((i,j)) for i in range(pixel_hg3[0],1+pixel_bd3[0]) for j in range(pixel_hg3[1],1+pixel_bd3[1])]
    couleurs4=[im.getpixel((i,j)) for i in range(pixel_hg4[0],1+pixel_bd4[0]) for j in range(pixel_hg4[1],1+pixel_bd4[1])]
    return (Bloc(couleur_moyenne(couleurs1),pixel_hg1,pixel_bd1),Bloc(couleur_moyenne(couleurs2),pixel_hg2,pixel_bd2),Bloc(couleur_moyenne(couleurs3),pixel_hg3,pixel_bd3),Bloc(couleur_moyenne(couleurs4),pixel_hg4,pixel_bd4))

def ouvre_image(image:str):
    """ouvre et convertit en formay RGB l'image prise en paramètres

    Précondition : le chemin vers l'image est correct 

    """
    img= Image.open(image)
    img.convert('RGB')
    return img
    
def dessine_blocs(dessin,bloc:Bloc):
    """dessine le bloc dans l'image

    Précondition : 
    

    """
    
    if len(bloc.sous_blocs)==0:
        dessin.rectangle((bloc.pixel_hg,bloc.pixel_bd),fill = bloc.couleur)
    else :
        for elem in bloc.sous_blocs:
            dessine_blocs(elem)
    
def dessine(img,bloc:Bloc):
    """associe à un bloc une image bitmap le représentant

    Précondition : l'image est ouverte 
    """
    dessin = ImageDraw.Draw(img)
    dessine_blocs(dessin,bloc)
    


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print("usage: decoupe.py operation image ordre")
    else:
        operation, image, ordre= sys.argv[1:]
        if operation in {"affiche", "enregistre"}:
            ordre = int(ordre)
            im_rgb=ouvre_image(image)
            t=im_rgb.size
            hg=(0,0)
            bd=(t[0]-1,t[1]-1)
            b=decoupe(im_rgb,hg,bd,ordre)
            dessin=dessine(im_rgb,b)
            
            if operation == "affiche":
                im_rgb.show()
            else:
                im_rgb.save("images/img_rec.png", "PNG")


