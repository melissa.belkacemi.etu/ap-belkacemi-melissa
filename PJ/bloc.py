#Belkacemi Melissa
#31/03/2024
from couleur import *

class Bloc:
    """ une classe représentant des blocs

    """
    def __init__(self,couleur:tuple[int,int,int],
                 pixel_hg:tuple[int,int],
                 pixel_bd:tuple[int,int],
                 *sous_blocs)->"Bloc":
        """

        Précondition : 
        Exemple(s) :
        $$$ b=Bloc((45,63,125),(120,23),(45,111))
        $$$ b.couleur
        (45,63,125)
        $$$ b.pixel_hg
        (120,23)
        $$$ b.pixel_bd
        (45,111)

        """
        self.pixel_hg=pixel_hg
        self.pixel_bd=pixel_bd
        self.sous_blocs=[]
        if len(sous_blocs)==0:
            self.couleur=couleur
        else:
            self.sous_blocs=sous_blocs
            couleurs=[sous_blocs[0].couleur,sous_blocs[1].couleur,sous_blocs[2].couleur,sous_blocs[3].couleur]
            self.couleur=couleur_moyenne(couleurs)
                
        
    def sont_proches(c1:tuple[int,int,int],c2:tuple[int,int,int],c3:tuple[int,int,int],c4:tuple[int,int,int]) ->bool:
        """Renvoie True si les couleurs des 4 blocs sont proches
        Précondition : les couleurs sont correctes
        Exemple(s) :
        $$$ b1=Bloc((1,2,3),(0,0),(1,1))
        $$$ b2=Bloc((0,2,3),(2,0),(3,1))
        $$$ b3=Bloc((0,0,3),(0,2),(1,3))
        $$$ b4=Bloc((10,123,34),(2,2),(3,3))
        $$$ b5=Bloc((0,1,3),(2,2),(3,3))
        $$$ b1.sont_proches(b2,b3,b4)
        False
        $$$ b1.sont_proches(b2,b3,b5)
        True

        """
        c1=self.couleur
        c2=bloc1.couleur
        c3=bloc2.couleur
        c4=bloc3.couleur
        cm=couleur_moyenne([c1,c2,c3,c4])
        return coul_sont_proches(c1,cm)and coul_sont_proches(c2,cm) and coul_sont_proches(c3,cm) and coul_sont_proches(c4,cm)

