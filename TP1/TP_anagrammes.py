#BELKACEMI MELISSA
# TP1 AP
# 24/01/2024


#Méthode split

#1
# s.split(' ') renvoie la liste ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# s.split('e') renvoie la liste ['la méthod', ' split ', 'st parfois bi', 'n util', '']
# s.split('é') renvoie la liste ['la m', 'thode split est parfois bien utile']
# s.split() renvoie la liste ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# s.split('') produit une erreur
# s.split('split') renvoie la liste ['la méthode ', ' est parfois bien utile']

#2 la méthode split permet de créer une liste de chaines à partir d'une chaine en la séparant à chaque occurence du séparateur ( qui doit être une chaine non vide) écrit entre les parentheses pour créer les éléments de la liste. Si on ne met pas de séparateur, le séparateur par défaut est ' ' 

#3 Cette méthode ne modifie pas la chaîne à laquelle elle s'applique

#Méthode join

#1
# "".join(l) renvoie la chaine 'laméthodesplitestparfoisbienutile'

# " ".join(l) renvoie la chaine 'la méthode split est parfois bien utile'

# ";".join(l) renvoie la chaine 'la;méthode;split;est;parfois;bien;utile'

# " tralala ".join(l) renvoie la chaine 'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'

# print("\n".join(l)) affiche :
#la
#méthode
#split
#est
#parfois
#bien
#utile

# "".join(s) renvoie la chaine 'la méthode split est parfois bien utile'

# "!".join(s) renvoie la chaine 'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'

# "".join() produit une erreur

# "".join([]) ''

# "".join([1,2]) produit une erreur

# la méthode join permet de créer une chaine à partir d'une chaine ou d'une liste de chaines en séparant chaque élément de la chaine ou de la liste de chaine par un séparateur de type str écrit entre les guillemets. Si on ne met pas de séparateur, le séparateur par défaut est ' '

#3 Cette méthode ne modifie pas la chaîne à laquelle elle s'applique

#4
def join(s:str,l_chaines:list[str])-> str:
    """Renvoie une chaîne de caractères construite en concaténant toutes les chaînes de l_chaine en intercalant s sans utiliser la méthode join

    Précondition :aucune
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    $$$ join('', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymondcalbuthronchinfr'
    $$$ join('.', [])
    ''
    """
    if len(l_chaines)!=0:
        res=l_chaines[0]
        for i in range(1,len(l_chaines)):
            res= res+s+ l_chaines[i]
    else :
        res=''
    return res

#Méthode sort
#1
# l = list('timoleon')
# l est devenue ['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']

#  l = list(s)
# l est devenue [' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']

# si s est une chaine de caractères quelconque,après application de la méthode sort, les caractères de l sont rangés par ordre alphabétique

#2 l = ['a', 1]
# l.sort() produit une erreur car on ne peut pas ranger des éléments qui ne sont pas du même type

# UNE FONCTION POUR LES CHAINES
def sort(s: str) -> str:
    """
    Renvoie une chaîne de caractères contenant les caractères de s triés dans l'ordre croissant.

    Précondition: aucune

    Exemples:
    $$$ sort('timoleon')
    'eilmnoot'
    """
    res=list(s)
    res.sort()
    res=''.join(res)
    return res

#Anagrammes

def sont_anagrammes_v1(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes_v1('orange', 'organe')
    True
    $$$ sont_anagrammes_v1('orange','Organe')
    False
    $$$ sont_anagrammes_v1('orange','orangé')
    False
    """
    s1_r=sort(s1)
    s2_r=sort(s2)
    return s1_r == s2_r

def sont_anagrammes_v2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes_v2('orange', 'organe')
    True
    $$$ sont_anagrammes_v2('orange','Organe')
    False
    $$$ sont_anagrammes_v2('orange','orangé')
    False
    """
    dict1=dict()
    dict2=dict()
    for i in range(len(s1)) :
        if s1[i] in dict1 :
            dict1[s1[i]]+=1
        else :
            dict1[s1[i]]=1
    for i in range(len(s2)) :
        if s2[i] in dict2 :
            dict2[s2[i]]+=1
        else :
            dict2[s2[i]]=1
    return dict1==dict2
 
def sont_anagrammes_v3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes_v3('orange', 'organe')
    True
    $$$ sont_anagrammes_v3('orange','Organe')
    False
    $$$ sont_anagrammes_v3('orange','orangé')
    False
    """
    i=0
    while i<len(s1) and s1.count(s1[i])==s2.count(s1[i]):
        i+=1
    return len(s1)==len(s2) and i==len(s1)

#Casse et accentuation
#1        
EQUIV_NON_ACCENTUE={'é':'e','è':'e', 'à':'a','â':'a','ê':'e','ë':'e','î':'i','ï':'i','ô':'o','ö':'o','ù':'u','û':'u','ü':'u','ÿ':'y','ç':'c'}

#2
def bas_casse_sans_accent(chaine:str)->str:
    """Renvoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué

    Précondition : aucune
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
 
    """
    res=''
    for elt in chaine :
        if elt.lower() in EQUIV_NON_ACCENTUE:
            res+= EQUIV_NON_ACCENTUE[elt.lower()]
        else:
            res+= elt.lower()
    return res
#1

def sont_anagrammes_v4(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes_v4('orange', 'organe')
    True
    $$$ sont_anagrammes_v4('orange','Organe')
    True
    $$$ sont_anagrammes_v4('orange','orangé')
    True
    $$$ sont_anagrammes_v4('Orangé', 'organE')
    True
    """
    chaine1=bas_casse_sans_accent(s1)
    chaine2=bas_casse_sans_accent(s2)
    i=0
    while i<len(s1) and chaine1.count(chaine1[i])==chaine2.count(chaine1[i]):
        i+=1
    return len(s1)==len(s2) and i==len(chaine1)

# Recherche d'anagrammes
from lexique import LEXIQUE

#1 len(LEXIQUE)= 139719
#2 il ne contient pas de doublons

# Anagrammes d'un mot : première méthode
#1

def anagrammes(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagrammes('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes('info')
    ['foin']
    $$$ anagrammes('Calbuth')
    []
    """
    res=[]
    for elt in LEXIQUE :
        if sont_anagrammes_v4(mot,elt):
            res.append(elt)
    return res

# anagrammes('chien') renvoie la liste ['chien', 'chiné', 'niche', 'niché']


# Anagrammes d'un mot : seconde méthode
#1  il n'est pas raisonnable de prendre les mots du lexique pour clés parce qu'il y aurait autant de listes qu'il y a de mot dans le lexique
#2
def cle(mot:str)->str :
    """Renvoie la clé que doit avoir un mot donné dans le dictionnaire

    Précondition : len(mot)>0
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'


    """
    res= bas_casse_sans_accent(mot)
    res=sort(res)
    return res
#1
def dictionnaire(liste:list[str])->dict :
    """Construit le dictionnaire des anagrammes, dont chaque association a pour clé la clé d'un mot du lexique, et pour valeur la liste des anagrammes

    Précondition :
    Exemple(s) :
    $$$ dictionnaire(['orange','organe','orangé'])
    {'aegnor': ['orange', 'organe', 'orangé']}

    """
    res={}
    for k in liste:
        if cle(k) in res :
            res[cle(k)].append(k)
        else :
            res[cle(k)]=[k]
    return res

ANAGRAMMES=dictionnaire(LEXIQUE)
# len(ANNAGRAMMES)=118625

#1
def anagrammes_v2(mot:str)->list :
    """Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition : aucune
    Exemple(s) :
    $$$ anagrammes_v2('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes_v2('info')
    ['foin']
    $$$ anagrammes_v2('Calbuth')
    [] 

    """
    res=ANAGRAMMES[cle(mot)]
    return res
    
#2 anagrammes_v2('chien') renvoie ['chien', 'chiné', 'niche', 'niché']

# Comparaison des deux méthodes
#for i in range(30):
#    r2=len(anagrammes_v2(LEXIQUE[i]))
#    print(r2)
#for i in range(30):
#    r1=len(anagrammes(LEXIQUE[i]))
#    print(r1)

# la version la plus rapide est la deuxième

# Phrases d'anagramme

def anagrammes_phrase(chaine:str)->list[str]:
    """Prend une phrase et renvoie la liste des phrases obtenues en remplaçant chacun des mots par leurs anagrammes.

    Précondition : 
    Exemple(s) :
    $$$ anagrammes_phrase('Mange ton orange')
    ['mange ont onagre', 'mange ont orange', 'mange ont orangé', 'mange ont organe', 'mange ont rongea', 'mange ton onagre', 'mange ton orange', 'mange ton orangé', 'mange ton organe', 'mange ton rongea', 'mangé ont onagre', 'mangé ont orange', 'mangé ont orangé', 'mangé ont organe', 'mangé ont rongea', 'mangé ton onagre', 'mangé ton orange', 'mangé ton orangé', 'mangé ton organe', 'mangé ton rongea']

    """
    l=chaine.split(' ')
    l_anagrammes=[]
    for elt in l:
        l_anagrammes.append(anagrammes_v2(cle(elt)))
    l_anagrammes_phrases=l_anagrammes[0]
    for liste in l_anagrammes[1:]:
        combinaison=[]
        for mot in l_anagrammes_phrases :
            for m in liste:
                combinaison.append(mot+' '+m)
        l_anagrammes_phrases=combinaison
    return  l_anagrammes_phrases



