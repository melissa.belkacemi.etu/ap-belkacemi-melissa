# TP7 AP Analyse empirique des tris
# Belkacemi Melissa
# 20/03/2024

#Préliminaires

# l'expression  l=[k for k in range(n)] permet de construire en compréhension une liste des entiers de 0 à n-1
from random import shuffle

def liste_alea(n:int)->list[int]:
    """Renvoie une liste de longueur n contenant les entiers de 0 à n-1 mélangés

    Précondition : n>=0
    Exemple(s) :
    $$$ liste_alea(0)
    []
    $$$ liste_alea(1)
    [0]

    """
    l=[k for k in range(n)]
    shuffle(l)
    return l
#Représentation graphique avec Matplotlib
import matplotlib.pyplot as plt

#Compter les comparaisons
from ap_decorators import count
from compare import compare

nvle_compare = count(compare)

#Évaluation expérimentale de la complexité en temps
import timeit

#Cas du tri par selection

TAILLE_MAX = 100
"""
c_select = [0.0] * (TAILLE_MAX + 1)
for t in range(TAILLE_MAX + 1):
    c_select[t] = timeit.timeit(stmt='tri_select(l)', setup=f'from tp7 import liste_alea ;l=liste_alea({t});from tris import tri_select', number=5000)


plt.plot(list(range(TAILLE_MAX + 1)), c_select, 'b.', label='Tri sélection')

plt.title('Tris : temps de comparaisons')
plt.legend()
plt.xlabel('n = taille des listes')
plt.ylabel('c(n) = temps de comparaisons')
plt.savefig('tris_select_tmpscomp.png')
plt.show()
"""
#Cas du tri par insertion
globals=globals()
#meilleur des cas
meil_cas=[0.0] * (TAILLE_MAX + 1)
for t in range(TAILLE_MAX + 1):
    liste=liste_alea(t)
    tmp=timeit.timeit(stmt=f'tri_insert({liste})', setup='from tris import tri_insert', number=5000)
    meil_cas[t]=tmp

    
    
    
    


