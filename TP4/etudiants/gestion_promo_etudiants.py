#belkacemi melissa
#TP4 Gestion d une promotion
#7/2/2024

#Préliminaire

def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous([True, True, True])
    True
    $$$ pour_tous([True, False, True])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]:
        i+=1
    return i==len(seq_bool)

def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False
    $$$ il_existe([False, False,True])
    True
    """
    i=0
    while i<len(seq_bool) and not seq_bool[i]:
        i+=1
    return i<len(seq_bool)
