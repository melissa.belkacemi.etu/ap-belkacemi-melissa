#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:author: FIL - FST - Univ. Lille.fr <http://portail.fil.univ-lille.fr>_
:date: janvier 2019
:last revised: 
:Fournit :
"""
from date import Date

#Question 1
class Etudiant:
    """
    une classe représentant des étudiants.

    $$$ etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ str(etu)
    'Tim Oléon'
    $$$ repr(etu)
    '314159 : Tim OLÉON'
    $$$ etu.prenom
    'Tim'
    $$$ etu.nip
    314159
    $$$ etu.nom
    'Oléon'
    $$$ etu.formation
    'MI'
    $$$ etu.groupe
    '15'
    $$$ etu2 = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu2
    True
    $$$ etu3 = Etudiant(141442, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu3
    False
    $$$ etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')
    $$$ etu < etu4
    True
    $$$ isinstance(etu.naissance, Date)
    True
    """
    
    def __init__(self, nip: int, nom: str, prenom: str,
                 naissance: Date, formation: str, groupe: str):
        """
        initialise un nouvel étudiant à partir de son nip, son nom, son
        prénom, sa formation et son groupe.

        précondition : le nip, le nom et le prénom ne peuvent être nuls ou vides.
        """
        self.nip=nip
        self.nom=nom
        self.prenom=prenom
        self.naissance=naissance
        self.formation=formation
        self.groupe=groupe

    def __eq__(self, other) -> bool:
        """
        Renvoie True ssi other est un étudiant ayant :
        - même nip,
        - même nom et
        - même prénom que `self`,
        et False sinon.
        """
        return (self.nip==other.nip)and(self.nom==other.nom)and(self.prenom==other.prenom)

    def __lt__(self, other) -> bool:
        """
        Renvoie True si self est né avant other
        """
        return self.naissance < other.naissance
        
        

    def __str__(self) -> str:
        """
        Renvoie une représentation textuelle de self.
        """
        return f"{self.prenom} {self.nom}"

    def __repr__(self) -> str:
        """
        Renvoie une représentation textuelle interne de self pour le shell.
        """
        
        return f"{self.nip} : {self.prenom} {(self.nom).upper()}"
    
# Question 2    
def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
        """
        Renvoie la liste des étudiants présents dans le fichier dont
        le nom est donné en paramètre.

        précondition: le fichier est du bon format.
        """
        res = []
        with open(fname, 'r') as fin:
            fin.readline()
            ligne = fin.readline()
            while ligne != '':
                nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
                y, m, d = naissance.split('-')
                date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
                res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
                ligne = fin.readline()
        return res    
    
L_ETUDIANTS=charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE=[L_ETUDIANTS[i] for i in range(10)]

# Question 3
def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    l=[type(x[i])==Etudiant for i in range(len(x))]
    return all(l)

#Gestion de la promotion

# Question 4
# NBRE_ETUDIANTS==603
NBRE_ETUDIANTS=len(L_ETUDIANTS)

# Question 5
NIP=42318104
#NIP%NBRE_ETUDIANTS==167
#L_ETUDIANTS[NIP%NBRE_ETUDIANTSv]==48730740 : Henri SAUVAGE

#Question 6
L_ETUDIANTS_MOIS_VING=[L_ETUDIANTS[i] for i in range(NBRE_ETUDIANTS) if Date(L_ETUDIANTS[i].naissance.jour,L_ETUDIANTS[i].naissance.mois,L_ETUDIANTS[i].naissance.annee)>Date(2, 2, 2004)]

#Question 7

def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    return {liste[i].formation for i in range(len(liste))}

#Question 8

def dico_cpt_prenom(liste:list[Etudiant]) -> dict:
    """Renvoie un dictionnaire qui associe les prenoms des étudiants de la liste et leurs nombres d'occurences dans cette liste

    Précondition : 
    Exemple(s) :
    $$$ dico_cpt_prenom(COURTE_LISTE)
    {'Eugène': 1, 'Josette': 1, 'Benoît': 1, 'David': 2, 'Andrée': 1, 'Cécile': 1, 'Christiane': 1, 'Paul': 1, 'Anne': 1}

    """
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res
dico_prenoms=dico_cpt_prenom(L_ETUDIANTS)

# dico_prenoms['Alexandre'] vaut 3
#il y a 3 personnes qui se prénomment Alexandre
# dico_prenoms['Camille'] vaut 2
#il y a 2 personnes qui se prénomment Alexandre

# cette fonction parcoure la liste une fois

#Question 9
# pour trouver le nombre de prénoms différents : len(dico_prenoms)
# il y a 198 prénoms différents

if (__name__ == "__main__"):
    import apl1test
    apl1test.testmod('etudiant.py')
